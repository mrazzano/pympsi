"""

 Script name: PackageSuiteInstaller.py

 Description:
    This class contains The MultiPackageSuite installer

"""

import glob
import json
import logging
import platform
import random
import time
import tarfile
import shutil
import sys
import os
import zipfile

try:
    import urllib.request
except ImportError:
    import urllib2

# Some general information

__author__ = "Massimiliano Razzano"
__copyright__ = "Copyright 2016-2020, Massimiliano Razzano"
__credits__ = ["Line for credits"]
__license__ = "GPL"
__version__ = "0.2.0"
__maintainer__ = "M. Razzano"
__email__ = "massimiliano.razzano@pi.infn.it"
__status__ = "Production"


class PackageSuiteInstaller:
    """

    This class contains a Data Manager for Grativational Wave data

    """

    def __init__(self, m_log_level="INFO"):
        """

        """

        # general constants
        self.__COMMANDS_LIST = ["install", "list", "remove", "setup", "upgrade"]

        self.__COMMANDS_HELP = {"list": "List the available packages and libraries",
                                "install": "Install a package or a library (install package_name or extlib_name)",
                                "remove": "Remove a package of a library (install package_name or extlib_name)",
                                "setup": "Just setup the Virtual Environment",
                                "upgrade": "Upgrade the package"}

        self.__ITEM_CLASSES = ["package groups", "packages", "external libraries"]
        self.__BASE_LIBS_WINDOWS = ["numpy-1.11.1+mkl-cp34-cp34m-win32.whl", "scipy-0.18.0-cp34-cp34m-win32.whl"]
        self.__PYTHON_OFFICIAL_FTP = "https://www.python.org/ftp/python"

        # general variables
        self.__installation_dir = ""
        self.__installation_mode = ""
        self.__temp_dir = None
        self.__extlib_dir = ""
        self.__venv_dir = ""
        self.__test_mode = False
        self.__use_system_python = False
        self.__repos_list = []
        self.__repository_location = ""
        self.__repository_type = None
        self.__repository_data = None
        self.__suite_name = ""
        self.__python_dir = None
        self.__python_version = None
        self.__pip_command = None
        self.__setup_script_name = ""
        self.__setup_script = ""

        self.__start_time = str(time.strftime("%y %b %Y %Hh%Mm%Ss", time.localtime()))

        # Init logger
        self.__logger = logging.getLogger()
        base_handler = logging.StreamHandler()
        # formatter = logging.Formatter('%(asctime)s %(levelname)-4s: %(message)s', datefmt='%H:%M:%S')
        formatter = logging.Formatter('%(levelname)-4s: %(message)s', datefmt='%H:%M:%S')

        base_handler.setFormatter(formatter)
        self.__logger.addHandler(base_handler)

        if m_log_level == "DEBUG":
            self.__logger.setLevel(logging.DEBUG)
        else:
            self.__logger.setLevel(logging.INFO)

        self.__logger.info("Python Multi Package Suite Installer")
        self.__logger.info(
            "Starting time:  " + self.__start_time + " (LEVEL=" + str(self.__logger.getEffectiveLevel()) + ")")

        # Init configuration
        self.__logger.info("Initializing Installer configuration...")

        self.__os_system = platform.system()
        self.__release = platform.release()
        self.__machine = platform.machine()
        self.__running_python_version = float(sys.version[:3])

        # short fix for MAC OS
        if (self.__os_system == "Darwin"):
            self.__logger.info("Recognized Mac OS system: " + platform.system() + " " + self.__release)
            self.__logger.info("Converting to Linux...")
            self.__os_system = "Linux"
            self.__use_system_python = True

        if self.__os_system == "Windows":
            self.__use_system_python = True
        self.__logger.info("Operating System: " + self.__os_system)
        self.__logger.info("Release: " + self.__release)
        self.__logger.info("Machine type: " + self.__machine)
        self.__logger.info("Running Python " + str(self.__running_python_version))

        if self.__use_system_python:
            self.__logger.info("Using Python installed on the system")

        # Get the shell type
        self.__shell_type = None
        if self.__os_system == "Linux":
            os.system("echo $0 >> myshell.txt")
            self.__shell_type=open("myshell.txt").readlines()[0]
            self.__shell_type = os.environ['SHELL']
            if os.path.basename(self.__shell_type) == "bash":
                self.__shell_type = "sh"
            elif os.path.basename(self.__shell_type) == "tcsh":
                self.__shell_type = "csh"
        elif self.__os_system == "Windows":
            self.__shell_type = "bat"

        #print(self.__shell_type)
        #sys.exit(9)

    def __del__(self):
        self.__logger.debug("Deleting temporary data...")
        if os.path.exists(self.__temp_dir):
            shutil.rmtree(self.__temp_dir)

        self.__logger.info("In order to setup the packages in the " + self.__suite_name + "suite, please type:")
        if self.__os_system == "Linux":
            self.__logger.info("source " + self.__setup_script_name)
        elif self.__os_system == "Windows":
            self.__logger.info("call " + self.__setup_script_name)

    def add_repository(self, m_repo_location):
        """

        :param m_repo_location:
        :return:
        """

        self.__repository_location = m_repo_location
        self.__logger.info("Using repository " + m_repo_location)

    def ask_question_yesno(self, m_question):
        """

        :param m_question:
        :return:
        """
        ans = ''
        while (ans != 'Y') and (ans != 'N'):
            if self.__running_python_version < 3.0:
                ans = raw_input(m_question)
            else:
                ans = input(m_question)

        return ans

    def create_temp_dir(self):
        """

        :return:
        """

        # If not temp dir exist, create one.
        if self.__temp_dir is None:
            self.__temp_dir = os.path.join(self.__installation_dir, ("temp%.6d" % random.uniform(0, 10000)))
            os.mkdir(self.__temp_dir)
            self.__logger.debug("Created Temp directory " + self.__temp_dir)

    def download_urllib(self, m_url, m_outfile):
        """

        """

        if self.__os_system == "Windows":
            m_url = m_url.replace("\\", "/")
        self.__logger.info("Downloading " + m_outfile + " from " + m_url)
        if self.__running_python_version < 3.0:
            downloaded_file = urllib2.urlopen(m_url)
            with open(m_outfile, 'wb') as output:
                output.write(downloaded_file.read())
        else:
            urllib.request.urlretrieve(m_url, m_outfile)

    def format_string_echo(self, m_string, m_shell_type=None,m_verbose=True):
        """

        :param m_string:
        :param m_shell_type:
        :return:
        """

        out_string = ""

        if m_shell_type is None:
            m_shell_type = self.__shell_type

        if m_verbose:
            if m_shell_type == "sh" or m_shell_type == "csh":
                out_string = "echo \"" + m_string + "\"\n"
            elif m_shell_type == "bat":
                out_string = "echo OFF\necho \"" + m_string + "\"\n"
            else:
                raise RuntimeError("OS " + self.__os_system + " no recognized!")

        return out_string

    def format_string_setenv(self, m_var_name, m_var_value, m_shell_type=None, m_echo=False):
        """

        :param m_var_name:
        :param m_var_value:
        :param m_shell_type:
        :param m_echo:
        :return:
        """

        out_string = ""

        if m_shell_type is None:
            m_shell_type = self.__shell_type

        if m_shell_type == "sh":
            out_string = "export " + str(m_var_name) + "=" + str(m_var_value) + "\n"
        elif m_shell_type == "csh":
            out_string = "setenv " + str(m_var_name) + " " + str(m_var_value) + "\n"
        elif m_shell_type == "bat":
            out_string = "set " + str(m_var_name) + "=" + str(m_var_value) + "\n"
        else:
            raise RuntimeError("OS " + self.__os_system + " no recognized!")

        if m_echo:
            out_string += self.format_string_echo("Variable " + m_var_name + " set to " + str(m_var_value))

        return out_string

    def get_commands(self):
        """

        :return:
        """

        return self.__COMMANDS_LIST, self.__COMMANDS_HELP

    def get_item_type(self, m_name):
        """

        :param m_name:
        :return:
        """

        m_item_type = None

        for ci in self.__ITEM_CLASSES:
            try:
                available_items = self.__repository_data[ci]
                for ii in available_items.keys():
                    if str(ii) == m_name:
                        m_item_type = str(ci)
            except KeyError:
                pass

        return m_item_type

    def install(self, m_item_name, m_version="HEAD"):
        """

        :param m_item_name:
        :param m_version:
        :return:
        """

        m_item_type = self.get_item_type(m_item_name)

        if self.__installation_mode == "DEV" and (m_item_type != "external libraries"):
            m_version = "HEAD"
            self.__logger.info("We are in DEV mode, so versions are overriden to HEAD...")

        if m_item_type == "packages":
            self.install_package(m_item_name, m_version)
        elif m_item_type == "external libraries":
            self.install_extlib(m_item_name, m_version)
        else:
            self.__logger.critical(m_item_name + " not found")
            sys.exit(1)

    # self.__logger.info("Installation complete!")
    # self.__logger.info("Before starting, run the script " + self.__setup_script_name + "!")

    def install_extlib(self, m_extlib_name, m_extlib_version):
        """

        :param m_extlib_name:
        :param m_extlib_version:
        :return:
        """

        self.__logger.info("Installing library " + m_extlib_name + " (version " + str(m_extlib_version) + ")...")
        # retrieve info from the package
        m_extlib_data = self.retrieve_item_data("external libraries", m_extlib_name, m_extlib_version)

        m_extlib_source_name = str(m_extlib_data[self.__os_system + "_source"])
        m_extlib_url = os.path.join(str(self.__repository_data["HTTP_REPO_URL"]),
                                    str(self.__repository_data["EXTLIB_DIR"]))
        m_extlib_install_directory = os.path.join(self.__extlib_dir, m_extlib_name + "-" + m_extlib_version)

        if os.path.exists(os.path.join(os.path.join(self.__extlib_dir, "installed"),
                                       m_extlib_name + '_' + m_extlib_version + 'inst.log')) \
                and os.path.exists(m_extlib_install_directory):
            self.__logger.warning("Library already installed. Skip...")
            return 0

        os.chdir(self.__extlib_dir)
        # print os.path.join(self.__extlib_dir,m_extlib_source_name)
        self.download_urllib(os.path.join(m_extlib_url, m_extlib_source_name),
                             os.path.join(self.__extlib_dir, m_extlib_source_name))

        if self.__os_system == "Linux":
            self.untar_file(os.path.join(self.__extlib_dir, m_extlib_source_name), m_path="tmp" + m_extlib_name)
            os.chdir(glob.glob("tmp" + m_extlib_name + "/*")[0])

            # process configure directive
            m_config_directive = str(m_extlib_data[self.__os_system + "_configure_directive"])[1:-1].split(",")
            if len(m_config_directive) > 0:
                m_cmd = m_config_directive[0]
                if m_config_directive[1] == "--prefix":
                    m_cmd = m_cmd + " --prefix=" + m_extlib_install_directory
                self.run_command(m_cmd)

            # process install
            m_install_directive = str(m_extlib_data[self.__os_system + "_install_directive"])[1:-1].split(",")
            if len(m_install_directive) > 0:
                for m_ci in m_install_directive:
                    self.run_command(m_ci)

            # Clear the tmp directory
            shutil.rmtree(os.path.join(self.__extlib_dir, "tmp" + m_extlib_name))

        elif self.__os_system == "Windows":
            self.unzip_file(os.path.join(self.__extlib_dir, m_extlib_source_name), m_path=self.__extlib_dir)
            # unzip_file(extlib_source_name, m_path=".")

        # clean the downloaded file and write a log file.
        os.remove(os.path.join(self.__extlib_dir, m_extlib_source_name))

        # write a log
        m_inst_log = open(os.path.join(os.path.join(self.__extlib_dir, "installed"),
                                       m_extlib_name + '_' + m_extlib_version + 'inst.log'), 'w')
        m_inst_log.write(self.__start_time)
        m_inst_log.close()

        # append path
        self.__setup_script = open(os.path.join(self.__installation_dir, self.__setup_script_name), "a")
        self.__setup_script.write(
            self.format_string_setenv("PATH", os.path.join(m_extlib_install_directory, "bin:$PATH")))
        self.__setup_script.write(self.format_string_setenv("LD_LIBRARY_PATH", os.path.join(m_extlib_install_directory,
                                                                                            "lib:$LD_LIBRARY_PATH")))
        self.__setup_script.close()

    def install_package(self, m_pkg_name, m_pkg_version, m_install_dependencies=True):
        """

        :param m_pkg_name:
        :param m_pkg_version:
        :param m_install_dependencies:
        :return:
        """

        self.__logger.info("Installing " + m_pkg_name + " version " + m_pkg_version + "...")

        if self.__os_system == "Windows":
            self.__pip_command = os.path.join(os.path.join(self.__venv_dir, "Scripts"), "python -m pip")

        # retrieve info from the package
        m_pkg_data = self.retrieve_item_data("packages", m_pkg_name, m_pkg_version)
        os.chdir(self.__installation_dir)
        m_pkg_inst_dir = os.path.join(self.__installation_dir, m_pkg_name + "-" + m_pkg_version)

        if self.__installation_mode == "DEV":
            if os.path.exists(m_pkg_inst_dir):
                self.__logger.warning("Package directory already present!")
                ans = self.ask_question_yesno("Package already present!\nDo you want to delete it and reinstall? (Y/N)")
                if ans == "Y":
                    self.__logger.debug("Removing " + m_pkg_inst_dir)
                    shutil.rmtree(m_pkg_inst_dir)
                    m_cmd = "git clone " + m_pkg_data["git_url"] + " " + m_pkg_inst_dir
                    self.run_command(m_cmd)
            else:
                m_cmd = "git clone " + m_pkg_data["git_url"] + " " + m_pkg_inst_dir
                self.run_command(m_cmd)

        else:
            self.__logger.critical(self.__installation_mode + " mode not implemented yet!")
            sys.exit(1)

        # read the package requirements
        if m_install_dependencies:
            self.__logger.info("Processing  dependent packages for " + m_pkg_name + " version " + m_pkg_version)
            try:
                dependent_packages = open(
                    os.path.join(os.path.join(m_pkg_inst_dir, "config"), "pkg_requirements.txt")).readlines()
                # print dependent_packages
                if len(dependent_packages) > 0:
                    for di in dependent_packages:
                        if di[0] != "#":
                            self.install(di.split("==")[0], di.split("==")[1])
                            self.__setup_script = open(self.__setup_script_name, "a")
                            self.__setup_script.write(
                                self.format_string_setenv("PYTHONPATH", m_pkg_inst_dir + ":$PYTHONPATH"))
            except IOError:
                self.__logger.debug("No dependent suite package found")
        else:
            self.__logger.debug("Skip dependencies...")

        self.__setup_script = open(self.__setup_script_name, "a")
        self.__setup_script.write(self.format_string_setenv("PYTHONPATH", m_pkg_inst_dir + ":$PYTHONPATH"))
        self.__setup_script.write(self.format_string_setenv("PATH", os.path.join(m_pkg_inst_dir,"bin") + ":$PATH"))

        # Install dependent python packages
        if m_install_dependencies:
            self.__logger.info("Processing dependent Python modules for " + m_pkg_name)
            # read the script and execute a command
            # self.__setup_script.close()
            script_lines = open(self.__setup_script_name).readlines()
            script_exe_name = os.path.join(self.__installation_dir, "install_script." + self.__shell_type)
            script_exe = open(script_exe_name, "w")
            for si in script_lines:
                script_exe.write(si)
            script_exe.write(self.__pip_command + " install -r " + os.path.join(os.path.join(m_pkg_inst_dir, "config"),
                                                                                "requirements.txt\n"))
            #script_exe.write(self.__pip_command + " install -r " + os.path.join(os.path.join(m_pkg_inst_dir, ""),
            #                                                                    "requirements.txt\n"))

            # script_exe.write("deactivate")
            script_exe.close()
            if self.__os_system == "Windows":
                self.run_command("call " + script_exe_name)
            elif self.__os_system == "Linux":
                self.run_command(". " + script_exe_name)
            os.remove(script_exe_name)
        else:
            self.__logger.debug("Skip dependencies...")

        try:
            self.__logger.debug("Removing JSON package")
            os.remove(glob.glob(os.path.join(self.__installation_dir, "pkg_*" + m_pkg_name + "*.json"))[0])
        except IndexError:
            pass

    def install_python(self, m_version, m_os_system="Linux"):
        """

        :param m_version:
        :param m_os_system:
        :return:
        """

        self.__logger.info("Install local version of Python (version " + m_version + " for " + self.__os_system + ")")
        # sys.exit(9)
        # download version
        extlib_url = os.path.join(self.__repository_data['HTTP_REPO_URL'], "python_source")
        python_source_file = "Python-" + m_version + ".tgz"
        extlib_url = os.path.join(os.path.join(self.__PYTHON_OFFICIAL_FTP, m_version), python_source_file)
        try:
            self.download_urllib(extlib_url, os.path.join(self.__installation_dir, python_source_file))
        except urllib2.HTTPError:
            self.__logger.error(
                "Python not found on the official Python page. Download the default version from Repository...")
            python_source_file = "Python_" + self.__os_system.lower() + "_" + m_version + ".tgz"
            extlib_url = os.path.join(self.__repository_data['HTTP_REPO_URL'], "python_source")
            extlib_url = os.path.join(extlib_url, python_source_file)
            self.download_urllib(extlib_url, os.path.join(self.__installation_dir, python_source_file))

        # For Linux, proceed with installation
        if self.__os_system == "Linux":
            # Now, untar the file
            self.untar_file(os.path.join(self.__installation_dir, python_source_file), self.__installation_dir)
            os.chdir(os.path.join(self.__installation_dir, "Python-" + m_version))
            #self.run_command("./configure --with-libs='bzip --prefix=" + os.path.join(self.__installation_dir, "python" + m_version))
            os.mkdir(os.path.join(self.__installation_dir, "python" + m_version))
            os.mkdir(os.path.join(os.path.join(self.__installation_dir, "python" + m_version),"lib"))
            self.run_command("./configure CFLAGS='-fPIC' --enable-shared --prefix=" + os.path.join(self.__installation_dir, "python" + m_version)+" LDFLAGS=\"-Wl,-rpath " +os.path.join(os.path.join(self.__installation_dir, "python" + m_version),"lib")+"\"")
            #sys.exit(9)
            self.run_command("make")
            self.run_command("make install")
            os.chdir(self.__installation_dir)
            shutil.rmtree(os.path.join(self.__installation_dir, "Python-" + m_version))
            os.remove(os.path.join(self.__installation_dir, python_source_file))
            self.__python_dir = os.path.join(self.__installation_dir, "python" + m_version)
            return self.__python_dir

        elif self.__os_system == "Windows":
            self.__logger.warning("For Windows, you should have Python already installed. If now, please install it.")

    def install_venv(self):
        """

        :return:
        """

        self.__venv_dir = os.path.join(self.__installation_dir,
                                       "venv_python" + str(self.__python_version[:3]))
        self.__logger.info("Installing Python Virtual Environment in " + self.__venv_dir)

        if not self.__use_system_python:
            self.__python_dir = os.path.join(self.__installation_dir,
                                             "python" + self.__python_version)

        # create the setup script
        self.__setup_script_name = "setup_" + self.__suite_name + "-" + \
                                   self.__installation_mode + "." + self.__shell_type
        self.__setup_script_name = os.path.join(self.__installation_dir, self.__setup_script_name)
        self.__setup_script = open(self.__setup_script_name, "w")
        self.__setup_script.write(
            self.format_string_echo("*****************************************************************"))
        self.__setup_script.write(self.format_string_echo("   Setup for " + self.__repository_data[
            "SUITE_DESCRIPTION"] + " Suite (" + self.__installation_mode + " mode)", m_shell_type=self.__shell_type))
        self.__setup_script.write(
            self.format_string_echo("*****************************************************************"))

        if self.__os_system == "Linux":
            if self.__use_system_python:
                self.__pip_command = "python -m pip"
                self.run_command("python -m venv " + self.__venv_dir)
            else:
                if int(str(self.__python_version[0]))==3:
                    self.__pip_command = os.path.join(os.path.join(self.__venv_dir, "bin"), "python3 -m pip")
                    self.run_command(os.path.join(self.__python_dir, "bin/python"+str(self.__python_version[0])) + " -m venv " + self.__venv_dir)
                elif int(str(self.__python_version[0])) == 2:
                    self.run_command("virtualenv -p "+os.path.join(self.__python_dir,"bin/python"+self.__python_version[0])+" "+self.__venv_dir)
                    self.__pip_command = os.path.join(os.path.join(self.__venv_dir, "bin"), "pip")
                    #self.run_command(os.path.join(self.__python_dir,"bin/python" + str(self.__python_version[0])) + " -m venv " + self.__venv_dir)

            self.run_command(self.__pip_command + " install --upgrade pip")
            self.__setup_script.write("source " + os.path.join(self.__venv_dir, "bin/activate\n"))
            self.__setup_script.write(self.format_string_setenv(self.__suite_name.upper() + "_DIR", self.__installation_dir,m_echo=True))

        elif self.__os_system == "Windows":
            if self.__use_system_python:
                self.run_command("python -m venv " + self.__venv_dir)

            self.__pip_command = os.path.join(os.path.join(self.__venv_dir, "Scripts"), "python3 -m pip")
            # self.__pip_command = os.path.join(os.path.join(self.__venv_dir, "Scripts"), "pip")

            self.run_command(self.__pip_command + " install --upgrade pip")

            # also download and install precompiled Numpy and scipy...
            m_extlib_url = os.path.join(str(self.__repository_data["HTTP_REPO_URL"]),
                                        str(self.__repository_data["EXTLIB_DIR"]))
            for bi in self.__BASE_LIBS_WINDOWS:
                self.__logger.info("Now installing base library " + bi)
                extlib_url = os.path.join(m_extlib_url, bi)
                os.chdir(self.__extlib_dir)
                # print(self.__pip_command)
                # sys.exit(0)
                self.download_urllib(extlib_url, os.path.join(self.__extlib_dir, bi))
                cmd = self.__pip_command + " install " + os.path.join(self.__extlib_dir, bi)
                self.run_command(cmd)
                os.remove(os.path.join(self.__extlib_dir, bi))
            self.__setup_script.write(
                "call " + os.path.join(self.__venv_dir, os.path.join("Scripts", "activate.bat\n")))
            self.__setup_script.write(
                self.format_string_setenv(self.__suite_name.upper() + "_DIR", self.__installation_dir))
        else:
            self.__logger.critical("Operating System " + self.__os_system + " not recognized!Exit")
            sys.exit(1)

    def list_repo_items(self, m_item_type="packages"):
        """
        :param m_item_type:
        :return:
        """

        self.__logger.info(" ")
        self.__logger.info(" ")
        self.__logger.info("** List of " + m_item_type + " available for installation:")
        self.__logger.info(" ")
        try:
            item_list = sorted(self.__repository_data[m_item_type])
            if len(item_list) > 0:
                for ii in item_list:
                    self.__logger.info(ii + " : " + self.__repository_data[m_item_type][ii])
            else:
                self.__logger.info("None")
        except:
            self.__logger.warning("None")

    def parse_repo_data(self, m_json_filename=None):
        """

        :param m_json_filename:
        :return:
        """
        file_to_parse = ""

        if m_json_filename is None:
            file_to_parse = self.__repository_location
        else:
            file_to_parse = m_json_filename

        self.__logger.debug("Parsing repository file " + str(m_json_filename))

        parsed_data = None

        try:
            parsed_data = json.loads(open(m_json_filename).read())
            self.__logger.debug("Repository data read successfully")
        except IOError:
            self.__logger.critical("Cannot read repository file " + m_json_filename)
            sys.exit(1)

        return parsed_data

    def read_repo_data(self, m_repo_location=None):
        """

        :param m_repo_location:
        :return:

        """

        if m_repo_location is None:
            m_repo_location = self.__repository_location

        file_to_parse = None

        if (m_repo_location[:4] == "http") or (m_repo_location[:3] == "www"):
            self.__logger.debug("Repository location is a remote file")
            self.__repository_type = "url"

            # create a temp dir and download to it
            self.create_temp_dir()
            self.download_urllib(m_repo_location, os.path.join(self.__temp_dir, os.path.basename(m_repo_location)))
            file_to_parse = os.path.join(self.__temp_dir, os.path.basename(m_repo_location))
        else:
            self.__logger.debug("Repository location is a local file")
            self.__repository_type = "local"
            file_to_parse = m_repo_location

        print("SSS "+file_to_parse)
        self.__repository_data = self.parse_repo_data(file_to_parse)

    def retrieve_item_data(self, m_type, m_name, m_version):
        """

        :param m_type:
        :param m_name:
        :param m_version:
        :return:
        """

        item_location = os.path.join(os.path.dirname(self.__repository_location),
                                     self.__repository_data["REPODATA_DIR"])
        item_repo_name = self.__repository_data["ITEM_PREFIX"][m_type] + "_" + m_name + "-" + m_version + ".json"
        # if needed, create a temp dir
        self.create_temp_dir()
        self.download_urllib(os.path.join(item_location, item_repo_name), os.path.join(self.__temp_dir, item_repo_name))

        parsed_data = json.loads(open(os.path.join(self.__temp_dir, item_repo_name)).read())

        return parsed_data

    def run_command(self, m_command):
        """

        :param m_command:
        :return:

        """

        self.__logger.debug("Running command " + m_command)
        os.system(m_command)

    def set_installation_dir(self, m_dir_name):
        """

        :param m_dir_name:
        :return:
        """

        self.__installation_dir = m_dir_name
        self.__logger.debug("Installation dir set to " + self.__installation_dir)

    def set_installation_mode(self, m_inst_mode, m_test_mode):
        """

        :param m_inst_mode:
        :param m_test_mode:
        :return:
        """

        self.__installation_mode = m_inst_mode
        self.__test_mode = m_test_mode
        if self.__test_mode:
            self.__logger.info("Running in TEST mode (do not do anything)")

    def set_loglevel(self, m_level):
        """

        :param m_level:
        :return:
        """

        if m_level == "DEBUG":
            self.__logger.setLevel(logging.DEBUG)

        self.__logger.info("Logger level set to " + m_level)

    def set_python_version(self, m_version):
        """

        :param m_version:
        :return:
        """

        if m_version is not None:
            self.__python_version = m_version
        else:
            self.__python_version = self.__repository_data["PYTHON_VENV_VERSION"]

    def setup(self):
        """

        :return:
        """

        # setup the installation dir and subdir...
        self.__suite_name = str(self.__repository_data["SUITE_NAME"])
        self.__logger.info("Setup for Package Suite " + self.__suite_name)

        #create a temp dir
        self.create_temp_dir()

        if os.path.exists(os.path.join(self.__installation_dir, "extlibs")) \
                and os.path.exists(os.path.join(self.__installation_dir,
                                                "venv_python" + str(self.__repository_data["PYTHON_VENV_VERSION"][0]))):
            ans = self.ask_question_yesno("Destination directory appears to be a good directory. Use this one? (Y/N)")
            if ans == "Y":
                pass
            else:
                if self.__installation_mode == "DEV":
                    self.__installation_dir = os.path.join(self.__installation_dir, self.__suite_name + "-DEV"+"-py"+str(self.__python_version[:3]))
                elif self.__installation_mode == "PROD":
                    self.__installation_dir = os.path.join(self.__installation_dir, self.__suite_name+"-py"+str(self.__python_version[:3]))
        else:
            if self.__installation_mode == "DEV":
                self.__installation_dir = os.path.join(self.__installation_dir, self.__suite_name + "-DEV"+"-py"+str(self.__python_version[:3]))
            elif self.__installation_mode == "PROD":
                self.__installation_dir = os.path.join(self.__installation_dir, self.__suite_name+"-py"+str(self.__python_version[:3]))

        # print(self.__installation_dir)
        # sys.exit(9)
        self.__extlib_dir = os.path.join(self.__installation_dir, "extlibs")
        self.__python_dir = os.path.join(self.__installation_dir,
                                         "python" + self.__repository_data["PYTHON_VENV_VERSION"])

        self.__venv_dir = os.path.join(self.__installation_dir,
                                       "venv_python" + str(self.__repository_data["PYTHON_VENV_VERSION"][0]))

        if self.__os_system == "Linux":
            if self.__use_system_python:
                self.__pip_command = os.path.join(os.path.join(self.__python_dir, "bin"), "python -m pip")
            else:
                self.__pip_command = os.path.join(os.path.join(self.__python_dir, "bin"), "python3 -m pip")

        # print(glob.glob(os.path.join(self.__installation_dir,"setup*"+self.__shell_type)))
        if len(glob.glob(os.path.join(self.__installation_dir, "setup*" + self.__shell_type))) == 0:
            self.__setup_script_name = "setup_" + self.__suite_name + "-" + \
                                       self.__installation_mode + "." + self.__shell_type
        else:
            self.__setup_script_name = (
                glob.glob(os.path.join(self.__installation_dir, "setup*" + self.__shell_type))[0])

        # check if exist or not
        self.__logger.debug("Creating Directory Structure")
        if os.path.exists(self.__installation_dir):
            ans = self.ask_question_yesno(
                "Installation dir present!\nDo you want to delete it (Y) or keep the existing one (N)? (Y/N)")
            print(ans)
            if ans == "Y":
                self.__logger.debug("Removing existing installation...")
                shutil.rmtree(self.__installation_dir)

            else:
                self.__logger.info("Installation directory already present")
                self.__logger.info("Setup done!")
                return 0

        # Build the directory structure
        os.mkdir(self.__installation_dir)
        self.__logger.debug("Installation directory created")
        os.mkdir(self.__extlib_dir)
        self.__logger.debug("External Libraries directory created")
        os.mkdir(os.path.join(self.__extlib_dir, "installed"))

        # Install Python
        if not self.__use_system_python:
            ps_python_dir = self.install_python(self.__python_version, self.__repository_data["PYTHON_VENV_VERSION"])

        # Virtual Environment
        #sys.exit(0)
        self.install_venv()
        self.__logger.info("Setup done!")

    def untar_file(self, m_tarfilename, m_path="."):
        """

        :param m_tarfilename:
        :param m_path:
        :return:
        """

        if m_tarfilename.endswith("tar.gz") or m_tarfilename.endswith("tgz"):
            tar = tarfile.open(m_tarfilename)
            tar.extractall(path=m_path)
            tar.close()
            print("Extracted in Directory " + m_path)
        else:
            print("Not a tar.gz file: '%s '" % sys.argv[0])

    def unzip_file(self, m_zipfilename, m_path="."):
        """

        :param m_zipfilename:
        :param m_path:
        :return:
        """

        m_zipfile = zipfile.ZipFile(m_zipfilename, "r")
        m_zipfile.extractall(path=m_path)
        m_zipfile.close()
