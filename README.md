#           Readme

The **Python Multi Package Suite Installer** (pympsi) is a small Python tool that can manage the installation of a software suite composed by Python packages and external libraries.

##          Documentation

The documentation (created using Sphinx) is available on Read The Docs at [this link](http://pympsi.readthedocs.io)

##          Release Notes

** Initial import ( Oct. 26, 2016) **

* Initial import, from old code

** Version 0.1.0 ( Nov 4, 2016) **

* Initial tag
* First working version
* Added initial documentation on RTD


** Version 0.2.0 ( Dec 1, 2016) **

* Made the tool public
* Updated the documentation
