#!/usr/bin/env python

"""

 Script name: mps-installer.py

 Description:
 Python Multipackage suite installer
"""

import os
import sys

from optparse import OptionParser

__author__ = "Massimiliano Razzano"
__copyright__ = "Copyright 2016-2020, Massimiliano Razzano"
__credits__ = ["Line for credits"]
__license__ = "GPL"
__version__ = "0.2.0"
__maintainer__ = "M. Razzano"
__email__ = "massimiliano.razzano@pi.infn.it"
__status__ = "Production"

# Get Script name and path
ScriptName = os.path.split(sys.argv[0])[1].split('.')[0]
script_path = os.path.join(os.getcwd(), os.path.dirname(sys.argv[0]))

# Append the lib dir for importing classes
if script_path[-1] == ".":
    script_path = script_path[:-1]

lib_dir = os.path.join(os.path.dirname(script_path), "lib")
sys.path.append(lib_dir)

from packagesuiteinstaller import *

# Init the Package Installer
mpsi = PackageSuiteInstaller()

# Get list of commands and help
command_list, commands_help = mpsi.get_commands()

#######################################################
# Main
#######################################################

if __name__ == '__main__':

    usg = "\033[1;31m%prog [ options] command\033[1;m \n"

    desc = "\033[34mPython Multi Package Suite Installer\033[0m \nAvailable commands are: "
    for ci in command_list:
        desc += ci + ", "
    desc = desc[:-2]

    parser = OptionParser(description=desc, usage=usg)
    parser.add_option("-i", "--instDir", default="current",
                      help="Installation Dir (If none, the packages will be installed in the current directory")
    parser.add_option("-v", "--version", default="HEAD", help="Package version (HEAD is the latest available)")
    parser.add_option("-m", "--mode", default="DEV", help="Installation mode (DEV=Development, PROD=Production)")
    parser.add_option("-r", "--repository", default=None, help="Package Suite Repository (URL or directory)")
    parser.add_option("-P", "--pythonversion", default=None,
                      help="Python Version (only for Linux). If None, the repository one will be used")
    parser.add_option("-t", "--test", default=False, action="store_true",
                      help="Testing mode (does not do anything)")
    parser.add_option("-d", "--debug", default=False, action="store_true",
                      help="Debug mode")

    (options, args) = parser.parse_args()

    if (len(args) == 0) or not (args[0] in command_list):
        error_msg = "Command not available!\n\nAvailable commands are:\n"
        for ci in command_list:
            error_msg += ci + "\t" + commands_help[ci] + "\n"
        error_msg += "\nType \"mps-installer.py -h\" for further help\n"
        parser.error(error_msg)

    # package_name = options.name
    install_dir = options.instDir
    version = options.version
    mode = options.mode
    repository = options.repository
    test_mode = options.test
    debug = options.debug
    python_version = options.pythonversion

    command = args[0]
    package_name = None
    package_version = "HEAD"
    if len(args) > 1:
        package_name = args[1]
    if len(args) > 2:
        package_version = args[2]

    if debug:
        mpsi.set_loglevel("DEBUG")

    if install_dir == "current":
        install_dir = os.path.dirname(os.path.dirname(script_path))
        if os.path.basename(install_dir) == "pympsi":
            install_dir = os.path.dirname(install_dir)

    mpsi.set_installation_mode(mode, test_mode)
    mpsi.set_installation_dir(install_dir)
    mpsi.create_temp_dir()


    if repository is None:
        raise RuntimeError("No valid repository location entered!")

    # Add repository and read repo data
    mpsi.add_repository(repository)
    mpsi.read_repo_data()
    mpsi.set_python_version(python_version)

    # Now, process commands...
    if command == "list":
        # List packages or items in the repository
        for pi in ["groups of packages", "packages", "external libraries"]:
            mpsi.list_repo_items(m_item_type=pi)
    elif command == "setup":
        # Setup the directory...
        mpsi.setup()
    elif command == "install":
        # Setup the directory...
        mpsi.setup()
        mpsi.install(package_name, m_version=package_version)
