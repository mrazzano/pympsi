.. pympsi documentation
.. Here we put the general information

Features
************************

The pympsi installer has been designed to be flexible and customizable, in order to accomodate the installation of packages based mainly on Python, with the support of external libraries written in other languages (e.g. Fortran, C, C++).

These are the main features of the installer:

* Package information are on external repositories (local or remote)
* Highly customizable: installation parameters can be specified via the repository data
* Portable and platform-independent (as much as possible!)
* Creation of a custom Virtual Environment (based on Python version specified in the repository)
* Python module dependencies installation via PIP installer
* Management of dependencies among packages and external libraries of the software suite
* Download and installation of external libraries (e.g. C, C++)
