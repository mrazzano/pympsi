.. pympsi documentation


Using the installer
************************

To use the Python Multi Package Suite Installer, you have to run the mps-installer.py script under the bin directory:

.. code-block:: console

 ./pympsi/bin/mps-installer.py -h


**Note for Windows users**: you might need to run instead the command:

.. code-block:: console

 python pympsi\bin\mps-installer.py -h

Setup your package suite
========================
Here we assume that your package suite (let's call it *testsuite* is stored in a repository specified by the URL *http://www.pi.infn.it/~razzano/pympsi/testrepo/repo_test.json*.
You can first cd in the pympi directory:

 .. code-block:: console

  cd pympsi

The first action should be to setup the suite itself, by issuing the command

.. code-block:: console

 ./bin/mps-installer.py -m DEV -r http://www.pi.infn.it/~razzano/pympsi/testrepo/repo_test.json setup

This will create an environment under the directory *testsuite-DEV*, where you will find the virtuan environment (venv_pythonx.x) and (if you are running Linux) a Python version.
Also, in the testsuite-DEV you will find a setup script, called setup_testsuite-DEV.sh, that you will have to run to correctly set the envirnoment variables and to activate the Virtual Environment.

For instance, for Linux (bash/sh) shell, you can run:

.. code-block:: console

 source setup_vat.sh (or .csh)

For Windows systems, you can issue the command:

.. code-block:: console

 call setup_vat.bat


**Notes**
* You have two installation modes: DEV for developers and PROD for production. If you are developing your code, please use the DEV mode, so that you will be able to work on your packages, push, pull etc. Otherwise, if you want to deliver your code, just use the PROD mode.
* It is important that you use a valid repository, that you can specify with the -r option.


List available packages
========================

Once your suite has been set up, you can check which packages and external libraries are availabel, with this command:

.. code-block:: console

 ./pympsi/bin/mps-installer.py -m DEV -r http://www.pi.infn.it/~razzano/pympsi/testrepo/repo_test.json list

Install a package
========================

If you want to install the package *mypackage* with version *version*, you should run this command:

.. code-block:: console

 ./pympsi/bin/mps-installer.py -m DEV -r http://www.pi.infn.it/~razzano/pympsi/testrepo/repo_test.json install mypackage version

This will create a subdirectory mypackage-version.
**Note** when you are running in *development* mode, the versions will be set to HEAD.

In particular, it is important that you use a valid repository, that you can specify with the -r option:

.. code-block:: console

 ./pympsi/bin/mps-installer.py -r http://www.pi.infn.it/~razzano/pympsi/testrepo/repo_test.json install mypackage version


Summary of the available commands
========================

Here are the main commands. You have to specify the repository each time, running

.. code-block:: console

 ./pympsi/bin/mps-installer.py -r http://www.pi.infn.it/~razzano/pympsi/testrepo/repo_test.json command [options]


==================   ========================================   ============
Command              Command with Options                       Description
==================   ========================================   ============
help                 help -h                                    Getting help
install              install mypackage version                  Install *mypackage* with the specific *version*
list                 list                                       List the available packages for installation
setup                setup                                      Just setup the suite
==================   ========================================   ============

.. remove               remove mypackage          Remove the package (or package collection,or library) *mypackage*
