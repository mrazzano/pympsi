.. pympsi documentation


Requirements
************************

Before installing and running the pympsi installer package, you have to check your system for the following requisites, depending on your Operating System

Python
======================================

Python is a basic requirement to run the installer. In particular, Python 3 is strongly suggested, although version 2.7 is also supported.

To check for your Python version, simply type:

.. code-block:: console

 python -V


**Note for Windows users**:

* If you are working on a Windows machine, we ask you to install Python first on your system.
* Once you have installed Python, make sure that you add the \Scripts directory to your %PATH% enviroment variable. To learn how to do it, you can follow
`This link <https://technet.microsoft.com/en-us/library/cc736637%28v=ws.10%29.aspx/>`_

Python Package Installer (PIP)
======================================

Python 3 comes with the Python Package Installer (PIP): to be sure that you have PIP installed, type:

.. code-block:: console

 pip -V

It is recommended that you have the latest version of PIP installed. If not, you can upgrade it:

.. code-block:: console

 python3 -m pip install --upgrade pip  (Python 3.x)

 pip install --upgrade pip  (Python 2.7)

**Note on SSL/TSL libraries**:
When using PIP, you should have the SSL/TSL library installed. For windows, these are contained in the package libssl-dev.

GIT
======================================

In case you want to install packages thare are under GIT repositories, you should have GTI installed.
You can find more info on Git at `this page <https://git-scm.com>`_


**Note for Windows users**:
To download GIT for windows, you can use this link (https://git-scm.com/download/win)

SSH
======================================
In order to use PIP and GIT to access to private repositories, you should have an SSH client installed.
To check for your version of SSH, please type:

.. code-block:: console

 ssh -V

Linux systems should have it already, while for windows, you can download various clients. One possible option is
`OpenSSH for Windows <https://sourceforge.net/projects/opensshwindows//>`_

