.. pympsi documentation

Installation
************************

In order to install pympsi, you have two solutions: you can install from the GIT repository or using PIP.


Installing with GIT
======================================

You can get the latest version of the pympsi package from `the GIT repository <https://gitlab.com/mrazzano/pympsi>`_.
You simply have to git clone the project with this command:

.. code-block:: console

 git clone https://gitlab.com/mrazzano/pympsi.git

This will create a directory called pympsi under your current directory. In this directory you will find the code needed to run the installer.

Installing with PIP
======================================

This will be implemented soon. Stay tuned!


