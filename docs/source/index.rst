.. pympsi documentation master file, created by
   sphinx-quickstart on Thu Nov  3 17:59:10 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pympsi's documentation!
======================================

In these pages you can find the documentation related to the **Python Multi Package Suite Installer (pympsi)**.
The **Python Multi Package Suite Installer (pympsi)** is a small Python tool that can manage the installation of a software suite composed by Python packages and external libraries.
The installer is based on external repositories, so you can easily create your installation environment by just creating a custom repository for your package suite.

Introduction
======================================

.. toctree::
   :maxdepth: 3

   features.rst
   platforms.rst
   repository.rst
   license.rst

Getting Started
======================================

.. toctree::
   :maxdepth: 3

   requirements.rst
   installation.rst

.. Use this tutorial http://matplotlib.org/sampledoc/getting_started.html#installing-your-doc-directory

Using the Python Multi Package Suite Installer
======================================
.. toctree::
   :maxdepth: 3

   usage.rst

Testing the Installer
======================================
.. toctree::
   :maxdepth: 3

   testrepo.rst
      .. newrepo.rst



.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

