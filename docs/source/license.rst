.. pympsi documentation

License
************************

The pympsi package is released under the GNU GENERAL PUBLIC LICENSE (Version 3, 29 June 2007). You can find more detail in the LICENSE.txt file in the root directory of the package.