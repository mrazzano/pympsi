.. pympsi documentation

Repository
************************

pympsi is hosted  on Gitlab at `this link <https://gitlab.com/mrazzano/pympsi>`_.
In this page you can see the code and submit issue requests to the developer, in order to notify bugs and ask for new features.

You can submit issues from `this page <https://gitlab.com/mrazzano/pympsi/issues>`_.