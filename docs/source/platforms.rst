.. pympsi documentation

Supported platforms
************************

This package is mainly developed for Linux users, however we provide limited support also for Windows and Mac users.

This is a quick list of platform supported:

* Linux: various distributions (kernel 2.6 and later)
* Windows: Windows 7 and later
* Mac:  OS X 10.9 Mavericks and later

**Note:** If you find any particular lack of support for your Operating System, notify us at `this page <https://gitlab.com/mrazzano/pympsi/issues>`_.
