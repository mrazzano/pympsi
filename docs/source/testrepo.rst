Testing the installer with a Test repository
************************

In order to test the Python Multi Package Suite Installer, we prepared a test repository, that allows to install a test suite with a basic package and a library.

Structure of the test repository
========================
The test repository is located at a remote URL specified in the data/testrepo/repo_test.json file.
You can see the structure of the repository by following `this link <http://www.pi.infn.it/~razzano/pympsi/testrepo/>`_

The repository contains the data required to install:

* The `pybaselib <http://www.gitlab.com/mrazzano/pybaselib/>`_ package, a basic collection of Python modules and utilities

Testing the installation
========================
In order to install the pybaselib package and test the pympsi installer, you can run the following command:

.. code-block:: console

 ./pympsi/bin/mps-installer.py -m DEV -r http://www.pi.infn.it/~razzano/pympsi/testrepo/repo_test.json install pybaselib

 If the installation is successfull, you will see the newly-created testsuite-DEV directory, containing the Python Virtual Environment and a folder called pybaselib-HEAD. Linux users will also find a folder with the compiled version of Python.
